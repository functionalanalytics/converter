import logging
from typing import Union
import pandas
import numpy

logger = logging.getLogger(__name__)


def convert(amounts: Union[pandas.DataFrame, list],
            rates: Union[pandas.DataFrame, list],
            exchange_rate: str,
            amount: str,
            amount_currency: str,
            exchange_currency: str,
            base_currency: str = "EUR"):

    if isinstance(amounts, list):
        amounts = pandas.DataFrame(amounts[1:], columns=amounts[0])
    if isinstance(rates, list):
        rates = pandas.DataFrame(rates[1:], columns=rates[0])

    # merge the exchange rates and the amounts
    amounts = amounts.merge(rates[[exchange_currency, exchange_rate]], how="left",
                            left_on=amount_currency, right_on=exchange_currency)

    # add exchange rate of 1 for the EUR amounts
    amounts.loc[amounts[amount_currency] == base_currency, exchange_rate] = 1

    # if there is any amount without a valid exchange rate, throw an error
    if amounts[exchange_rate].isna().any():
        raise Exception("No exchange rate found for some amounts!")

    # define the name of the converted amount column
    converted_ccy = base_currency + "_" + amount

    # calculate the converted amount
    amounts[converted_ccy] = amounts[amount] / amounts[exchange_rate]

    return [amounts.columns.to_list()] + amounts.values.tolist() if len(amounts.columns) > 1 else \
        amounts.columns.to_list() + amounts.values.tolist()
