import setuptools

setuptools.setup(
     name='converterapi',
     version='0.1',    
     author="David Kun",
     author_email="david.kun@functionalanalytics.nl",
     description="Simple currency converter",
     long_description="Functionality to convert the currency of amounts given exchange rates and a base currency",
     use_2to3=True,
     packages=setuptools.find_packages(),
     install_requires=['pandas']
 )